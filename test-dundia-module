#! /usr/bin/python3

import sys
import struct
from enum import Enum

import dundia

def padarr(arr,needed):
    needed -= len(arr)
    return arr + [None]*needed

from dataclasses import dataclass
@dataclass
class TestLevelInfo:
    offset: int
    name: str
    primary: str
    boss: str
    x: int
    y: int


def get_testing_info(f):
    results = []
    for line in f:
        arr = line.rstrip().split("\t")
        offsetstr, name, primary, boss, x, y = padarr(arr, 6) 
        offset = int(offsetstr, 16) - 4
        if x: x = int(x)-1
        if y: y = int(y)-1
        results.append(TestLevelInfo(offset, name, primary, boss, x, y))
    f.close()
    return results

def assert_same(expected, actual, description, level):
    if expected != actual:
        raise Exception(f'{level}: {description} didn\'t match:\n expected: "{expected}"\n actual:   "{actual}"')

def assert_none_or_same(expected, actual, description, level):
    if expected is None:
        return
    assert_same(expected, actual, description, level)


def parse_args():
    """ Parse arguments, return results of argparse.ArgumentParser.parse_args() """
    import argparse

    p = argparse.ArgumentParser(description="Run some checks on the dundia module",
            formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    p.add_argument('--test-data', metavar='TEST_DATA', 
            default="testing-data.tsv",
            type=open,
            help='datafile to process')
    p.add_argument('--official-data', metavar='OFFICIAL_DATA', 
            default="tokyo.dat",
            type=lambda x: open(x,"rb"),
            help='official Last Call BBS tokyo.dat file to process')
    args = p.parse_args()

    return args

def main():
    args = parse_args()

    testinfo = get_testing_info(args.test_data)
    testinfodict = {x.name:x for x in testinfo}

    puzzles = dundia.load(args.official_data)

    print("Validating")
    for idx, puzzle in enumerate(puzzles):
        test = testinfodict[puzzle.nicename()];
        assert_none_or_same(test.offset, puzzle.offset, "file offset", puzzle.nicename())
        assert_none_or_same(test.name, puzzle.nicename(), "level name", puzzle.nicename())
        assert_none_or_same(test.primary, puzzle.primary_name(), "primary", puzzle.nicename())
        assert_none_or_same(test.x, puzzle.column, "column", puzzle.nicename())
        assert_none_or_same(test.y, puzzle.row, "row", puzzle.nicename())
        print(f"{idx+1:2d}. {puzzle.nicename():40s} okay")

    return 0


if __name__ == '__main__':
    sys.exit(main())
