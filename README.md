Dungeons & Diagrams (tokyo.dat) File Format
===========================================

The Dungeons & Diagrams puzzles are stored `Content/tokyo.dat`. If you
purchased the game on Steam, that will be in your Steam installation directory
at `steamapps/common/Last Call BBS/Content/tokyo.dat`.

All numbers are integers. The only multi-byte integers are 32-bits and are
little-endian.

File
------

The file has a single header with the number of puzzles that follow.

| Offset |   Length | Format | Description                           |
| -----: | -------: | ------ | ------------------------------------- |
|      0 |        4 |  int32 | number of puzzles                     |
|      4 | variable | Puzzle | one or more puzzles, documented below |

The number of puzzles is 64 as shipped.

Puzzle
------

| Offset |   Length | Format     | Description              |
| -----: | -------: | ---------- | ------------------------ |
|      0 |        4 | int32      | length of title in bytes |
|      4 | variable | string     | title of the puzzle      |
|  4+len |       80 | PuzzleData | documented below         |

The title of the puzzle is likely limited to A through Z, command (,) and
hyphen-dash (-). See `content/fonts/fantasy_regular.png`.

PuzzleData
----------
| Offset | Length | Format | Description                                   |
| -----: | -----: | ------ | --------------------------------------------- |
|      0 |     64 | int8   | contents of puzzle in row-major order, 8 by 8 |
|     64 |      4 | int32  | primary monster type                          |
|     68 |      4 | int32  | solo monster type                             |
|     72 |      4 | int32  | solo monster column; zero indexed             |
|     76 |      4 | int32  | solo monster row; zero indexed                |


The puzzle contents are from this list:

| Value | Meaning            |
| ----: | ------------------ |
|     0 | open               |
|     1 | wall               |
|     2 | dead end (monster) |
|     3 | in room (treasure) |

The monster types are:

| Value | Directory | Description |
| ----: | --------- | ----------- |
|     0 | bear      | bugbear                 |
|     1 | chest     | animated chest (mimic)  |
|     2 | cultist   | cultist                 | 
|     3 | goat      | large demon             | 
|     4 | imp       | imp                     | 
|     5 | goblin    | goblin                  | 
|     6 | golem     | golem                   |
|     7 | demon     | small demon             | 
|     8 | insectoid | insect                  | 
|     9 | king      | undead warrior          | 
|    10 | lich      | undead wizard (lich)    |
|    11 | minotaur  | minotaur                |
|    12 | lookseer  | brain with eyes         | 
|    13 | ogre      | two-headed ogre (ettin) | 
|    14 | skeleton  | animated skeleton       |
|    15 | slime     | slime                   |
|    16 | squid     | squid-headed humanoid (illithid/mind-flayer) |
|    17 | kobold    | kobold                  | 

The directory is found in `Content/Packed/textures/tokyo/monsters`, one
directory per monster. Each contains two files: `dance.array.tex` and
`idle.array.tex`. I have not verified these!

The kobold is unused in the game.

All monsters in a puzzle are of the primary type with one exception.  If there
is a monster (value 2 in the puzzle) at the location specified by the column
and row, then that single monster will instead use the solo monster type.
Otherwise, the solo monster type is unused for a puzzle. Generally puzzles not
using the solo monster have a column and row of 0,0, but this is not always
true, and a solo monster might be valid in location 0,0 (the upper left
corner.)


Additional Stuff
----------------

This project also includes:

- `dundia.py` - Python 3 module for parsing tokyo.dat
- `show-puzzle` - displays puzzles from tokyo.dat to standard output
- `test-dundia-module` - does minimal testing of the `dundia` module
- `testing-data.tsv` - data used by test-dundia-module


```
$ ./show-puzzle 1
THE SECRET TUNNELS
  Monsters: goblin
  Boss: ogre at 1, 4
   ▗62415445▖
  4▐■   ■■†■▌
  4▐■   ■■ ■▌
  4▐■$  ■■ ■▌
  4▐■■■ ■   ▌
  3▐■‡■   ■ ▌
  4▐■ ■ ■ ■†▌
  2▐      ■■▌
  6▐†■■■†■■■▌
    ▀▀▀▀▀▀▀▀

$ ./show-puzzle 1 --wide
THE SECRET TUNNELS
  Monsters: goblin
  Boss: ogre at 1, 4
   ▗▄6▄2▄4▄1▄5▄4▄4▄5▖
  4▐▒▒      ▒▒██ ☻██▌
  4▐██      ██▒▒  ▒▒▌
  4▐▒▒$$    ▒▒██  ██▌
  4▐██▒▒██  ██      ▌
  3▐▒▒†☻▒▒      ▒▒  ▌
  4▐██  ██  ██  ██ ☻▌
  2▐            ▒▒██▌
  6▐ ☻▒▒██▒▒ ☻▒▒██▒▒▌
   ▝▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▘
```



Credit
------

Reverse engineered and documented by Alan De Smet, August 2022.

