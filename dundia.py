#! /usr/bin/python3


# This file is part of the dundia
# Copyright 2022 Alan De Smet
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import sys
import struct
from enum import Enum

MONSTER_NAMES = {
        0:  'bear',
        1:  'chest',
        2:  'cultist',
        3:  'goat',
        4:  'imp',
        5:  'goblin',
        6:  'golem',
        7:  'demon',
        8:  'insectoid',
        9:  'king',
        10: 'lich',
        11: 'minotaur',
        12: 'lookseer',
        13: 'ogre',
        14: 'skeleton',
        15: 'slime',
        16: 'squid',
        17: '??kobold??',
        }

class Cell(Enum):
    OPEN = 0
    WALL = 1
    DEADEND = 2
    ROOMNODE = 3

class Puzzle:
    def __init__(self, name, grid, column, row, primary, boss, offset=None):
        self.rawname = name
        self.grid = grid
        self.column = column
        self.row = row
        self.primary = primary
        self.boss = boss
        self.offset = offset

    def nicename(self):
        return self.rawname.replace("\n"," ").replace("  ", " ")

    def primary_name(self):
        return MONSTER_NAMES[self.primary]

    def boss_name(self):
        return MONSTER_NAMES[self.boss]

    def boss_is_present(self):
        return self.grid[self.row][self.column] == Cell.DEADEND

    def walls_in_row(self, row):
        return sum(x==Cell.WALL for x in self.grid[row])

    def walls_in_column(self, column):
        slice = [x[column] for x in self.grid]
        return sum(x==Cell.WALL for x in slice)

def expect(f, expected):
    if isinstance(expected, str):
        expected = expected.encode('ASCII')
    d = f.read(len(expected))
    if d != expected:
        print("Mismatch:")
        print("Found:   ", d)
        print("Expected:", expected)
        raise Exception("Mismatch")

def load_puzzle(f):
    curpos = f.tell()
    strlen = struct.unpack('<L', f.read(4))[0]
    name = f.read(strlen).decode('ASCII')
    rawgrid = f.read(64)
    primary, boss, column, row = struct.unpack('<LLLL', f.read(16))
    grid = []
    for rowstart in range(0,64,8):
        thisrow = list(rawgrid[rowstart:rowstart+8])
        thisrow = list([Cell(x) for x in thisrow])
        grid.append(thisrow)

    puz = Puzzle(name, grid, column, row, primary, boss, curpos)

    return puz

def load(f):
    num_puzzles = struct.unpack('<L', f.read(4))[0]
    levels = []
    for i in range(64):
        levels.append(load_puzzle(f))
    return levels


